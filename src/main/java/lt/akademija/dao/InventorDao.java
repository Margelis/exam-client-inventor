package lt.akademija.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import lt.akademija.entities.Inventor;

public class InventorDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}
	
	public void save(Inventor inventor) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(inventor))
			inventor = em.merge(inventor);
		em.persist(inventor);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Inventor inventor) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		inventor = em.merge(inventor);
		em.remove(inventor);
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Inventor> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Inventor> cq = cb.createQuery(Inventor.class);
			Root<Inventor> root = cq.from(Inventor.class);
			cq.select(root); // we select entity here
			TypedQuery<Inventor> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Inventor> findClientInventors(long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Inventor> inventorQuery = em.createQuery("SELECT i From Inventor i WHERE i.client.id = :id", Inventor.class);
		inventorQuery.setParameter("id", id);
		
		return inventorQuery.getResultList();

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
}
