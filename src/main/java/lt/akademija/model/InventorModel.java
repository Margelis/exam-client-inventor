package lt.akademija.model;

import java.io.Serializable;
import lt.akademija.entities.Inventor;

public class InventorModel implements Serializable {
	
	private static final long serialVersionUID = -2640514586827666915L;
	private Inventor currentInventor;
	
	public Inventor getCurrentInventor() {
		return currentInventor;
	}
	public void setCurrentInventor(Inventor currentInventor) {
		this.currentInventor = currentInventor;
	}

}
