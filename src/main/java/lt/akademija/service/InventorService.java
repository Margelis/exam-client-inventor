package lt.akademija.service;

import java.util.List;
import lt.akademija.dao.InventorDao;
import lt.akademija.entities.Inventor;

public class InventorService {
	private InventorDao inventorDao;

	public void save(Inventor inventor) {
		inventorDao.save(inventor);
	}

	public void delete(Inventor inventor) {
		inventorDao.delete(inventor);
	}

	public List<Inventor> findAll() {
		return inventorDao.findAll();
	}
	
	/**
	 * Finds selected client inventors
	 * @param id client ID
	 * @return inventors list
	 */
	public List<Inventor> findClientInventors(long id) {
		return inventorDao.findClientInventors(id);
	}

	public InventorDao getInventorDao() {
		return inventorDao;
	}

	public void setInventorDao(InventorDao inventorDao) {
		this.inventorDao = inventorDao;
	}

}
