package lt.akademija.service;

import java.util.List;

import lt.akademija.dao.ClientDao;
import lt.akademija.entities.Client;

public class ClientService {
	private ClientDao clientDao;

	public void save(Client newClient){
		clientDao.save(newClient);
	}
	
	public void delete(Client client){
		clientDao.delete(client);
	}
	
	public List<Client> findAll(){
		return clientDao.findAll(); 
	}
	
	/**
	 * checks if user already exist
	 * @param client - user to check
	 * @return true - when user not exist
	 */
	public boolean checkIfUserCanBeRegistered(Client client){
		return clientDao.checkIfUserCanBeRegistered(client);
	}

	public ClientDao getClientDao() {
		return clientDao;
	}

	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}
}
