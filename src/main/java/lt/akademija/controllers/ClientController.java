package lt.akademija.controllers;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import lt.akademija.entities.Client;
import lt.akademija.model.ClientModel;
import lt.akademija.service.ClientService;

public class ClientController {
	private static final Logger log = LogManager.getLogger(ClientController.class.getName());
	private ClientModel clientModel;
	private ClientService clientService;
	
	public void create(){
		clientModel.setCurrentClient(new Client());
		log.info("Selected to create new invendor");
	}
	
	/**
	 * Finds clients with maximum inventor size
	 * @return clients list, ordered by inventor size DESC
	 */
	public List<Client> findClientsWithMaxInventorDesc(){
		List<Client> clients = clientService.findAll();
		
		Collections.sort(clients, new Comparator<Client>() {
			@Override
			public int compare(Client o1, Client o2) {
				return String.valueOf(o1.getClientInventors().size()).compareTo(String.valueOf(o2.getClientInventors().size()));
			}
		});
		
		Collections.reverse(clients);
		
		return clients;
	}
	
	/**
	 * Finds clients with maximum inventor weight
	 * @return clients list, ordered by inventor weight DESC
	 */
	public List<Client> findClientsWithMaxInventorWeightDesc(){
		List<Client> clients = clientService.findAll();
		
		Collections.sort(clients, new Comparator<Client>() {
			@Override
			public int compare(Client o1, Client o2) {
				return String.valueOf(o1.calculateClientInventorsWeight()).compareTo(String.valueOf(o2.calculateClientInventorsWeight()));
			}
		});
		
		Collections.reverse(clients);
		
		return clients;
	}
	
	/**
	 * Saves client if client not exist
	 */
	public void save(){
		
		if(clientService.checkIfUserCanBeRegistered(clientModel.getCurrentClient())){
			clientService.save(clientModel.getCurrentClient());
			clientModel.setCurrentClient(null);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addClient').hide();");	
			log.info("New client saved");
		}else{
			log.warn("Client already exist");;
			FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Client already exist!"));
		}
	}
	
	public void cancel(){
		clientModel.setCurrentClient(null);
	}
	
	public void overview(Client client){
		clientModel.setCurrentClient(client);
	}
	
	public List<Client> findClients(){
		return clientService.findAll();
	}
	
	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}
}
