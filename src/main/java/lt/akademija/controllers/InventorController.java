package lt.akademija.controllers;

import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import lt.akademija.entities.Inventor;
import lt.akademija.model.ClientModel;
import lt.akademija.model.InventorModel;
import lt.akademija.service.InventorService;

public class InventorController {
	private static final Logger log = LogManager.getLogger(InventorController.class.getName());
	private InventorModel inventorModel;
	private InventorService inventorService;

	private ClientModel clientModel;

	public void create() {
		inventorModel.setCurrentInventor(new Inventor());
		log.info("Selected to create new invendor");
	}
	
	public void save() {
		Inventor inventor = inventorModel.getCurrentInventor();
		inventor.setClient(clientModel.getCurrentClient());
		inventor.setUpdateDate(new Date());

		inventorService.save(inventor);
		inventorModel.setCurrentInventor(null);
		log.info("Invendor saved");
	}

	public void cancel() {
		inventorModel.setCurrentInventor(null);
		log.info("Invendor canceled");
	}

	public void overview(Inventor inventor) {
		inventorModel.setCurrentInventor(inventor);
	}

	public List<Inventor> findClientInventors(long id) {
		return inventorService.findClientInventors(id);
	}

	public void delete(Inventor inventor) {
		inventorService.delete(inventor);
		log.info("Invendor " + inventor.getTitle() + " saved.");
	}

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public InventorModel getInventorModel() {
		return inventorModel;
	}

	public void setInventorModel(InventorModel inventorModel) {
		this.inventorModel = inventorModel;
	}

	public InventorService getInventorService() {
		return inventorService;
	}

	public void setInventorService(InventorService inventorService) {
		this.inventorService = inventorService;
	}
}
