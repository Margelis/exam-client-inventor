package lt.akademija.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Client implements Serializable {
	
	private static final long serialVersionUID = -1521832055830929022L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String lastName;
	private Date birthday;
	private String phone;
	private String clientType;
	
	@OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
	private List<Inventor> clientInventors;
	
	public Client (){
		
	}
	
	public double calculateClientInventorsWeight(){
		double weightCount = 0;
		for(Inventor i : clientInventors){
			weightCount += i.getWeight();
		}
		
		return weightCount;
	}
	
	public int calculateClientInventorsSize(){
		return clientInventors.size();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Inventor> getClientInventors() {
		return clientInventors;
	}

	public void setClientInventors(List<Inventor> clientInventors) {
		this.clientInventors = clientInventors;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

}
